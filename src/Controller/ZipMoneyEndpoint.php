<?php


namespace Drupal\commerce_zipmoney\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Psr\Log\LoggerInterface;

use Drupal\commerce_order\Adjustment;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\commerce_shipping\OrderShipmentSummaryInterface;
use Drupal\commerce_shipping\PackerManagerInterface;
use Drupal\commerce_shipping\ShipmentManagerInterface;

use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;

use zipMoney\Configuration;
use zipMoney\Api\CheckoutsApi;
use zipMoney\Model\CreateChargeRequest;
use zipMoney\Model\CreateCheckoutRequest;
use zipMoney\Api\ChargesApi;


class ZipMoneyEndpoint extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The packer manager.
   *
   * @var \Drupal\commerce_shipping\PackerManagerInterface
   */
  protected $packerManager;

  /**
   * The order shipment summary.
   *
   * @var \Drupal\commerce_shipping\OrderShipmentSummaryInterface
   */
  protected $orderShipmentSummary;

  /**
   * The shipment manager.
   *
   * @var \Drupal\commerce_shipping\ShipmentManagerInterface
   */
  protected $shipmentManager;

  /**
   * Constructs a PayPalCheckoutController object.
   *
   * @param \Drupal\commerce_paypal\CheckoutSdkFactoryInterface $checkout_sdk_factory
   *   The PayPal Checkout SDK factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger, MessengerInterface $messenger, PackerManagerInterface $packer_manager, OrderShipmentSummaryInterface $order_shipment_summary, ShipmentManagerInterface $shipment_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->packerManager = $packer_manager;
    $this->orderShipmentSummary = $order_shipment_summary;
    $this->shipmentManager = $shipment_manager;
    Configuration::getDefaultConfiguration()
      ->setApiKeyPrefix('Authorization', 'Bearer');
    Configuration::getDefaultConfiguration()->setPlatform('Drupal 8.9');

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('messenger'),
      $container->get('commerce_shipping.packer_manager'),
      $container->get('commerce_shipping.order_shipment_summary'),
      $container->get('commerce_shipping.shipment_manager')
    );
  }


  public function Checkout(PaymentGatewayInterface $commerce_payment_gateway, OrderInterface $order) {
    $config = $commerce_payment_gateway->getPluginConfiguration();

    Configuration::getDefaultConfiguration()
      ->setApiKey('Authorization',$config['api_private_key']);
    // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
    Configuration::getDefaultConfiguration()->setEnvironment($config['mode'] == 'live' ? 'production' : 'sandbox');

    // Configure API key authorization: Authorization
    $shopper = $this->getShopperDetails($order);
    $zip_order = $this->getOrderDetails($order);

    if ($shipping_profile = $order->getData('zipmoney_details')['shippingAddress']) {
      $zip_order["shipping"] = [
        "address" => [
          'line1' => $shipping_profile->get('address')->address_line1,
          'line2' => $shipping_profile->get('address')->address_line2,
          'city' => $shipping_profile->get('address')->locality,
          'state' => $shipping_profile->get('address')->administrative_area,
          'postal_code' => $shipping_profile->get('address')->postal_code,
          'country' => $shipping_profile->get('address')->country_code,
        ],
      ];
    }
    else {
      $zip_order["shipping"]['address'] = $shopper['billing_address'];
    }

    if($shipping_profile) {
      [$shipments, $removed_shipments] = $this->packerManager->packToShipments($order, $shipping_profile, []);
    }

    $redirect_url = Url::fromRoute('commerce_zipmoney.zipmoney_return', [
      'commerce_order' => $order->id(),
      'commerce_payment_gateway' => $commerce_payment_gateway->id(),
    ])->toString();

    global $base_url;

    $data = [
      'shopper' => $shopper,
      'order' => $zip_order,
      'config' => [
        'redirect_uri' => $base_url . $redirect_url,
      ],
    ];

    $api_instance = new CheckoutsApi();
    $body = new CreateCheckoutRequest($data); // \zipMoney\Model\CreateCheckoutRequest |

    try {
      $result = $api_instance->checkoutsCreate($body);
    } catch (Exception $e) {
      echo 'Exception when calling CheckoutsApi->checkoutsCreate: ', $e->getMessage(), PHP_EOL;
    }

    $order->setData('zip_shipments', $shipments);
    $order->setData('checkoutId', $result->getId());
    $payment_gateway = $commerce_payment_gateway;
    $order->set('payment_gateway', $payment_gateway);

    $order->save();

    return new JsonResponse([
      'id' => $result->getId(),
      'uri' => $result->getUri(),
      'redirect_uri' => $result->getUri(),
    ]);
  }


  public function Return(RouteMatchInterface $route_match, Request $request) {
    $result = $_GET['result'];
    $checkout_id = $_GET['checkoutId'];

    $commerce_payment_gateway =  $route_match->getParameter('commerce_payment_gateway');
    $config = $commerce_payment_gateway->getPluginConfiguration();

    Configuration::getDefaultConfiguration()
      ->setApiKey('Authorization', $config['api_private_key']);
    Configuration::getDefaultConfiguration()->setEnvironment($config['mode'] == 'live' ? 'production' : 'sandbox');

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    if ($checkout_id != $order->getData('checkoutId') || $result !== 'approved') {
      // Throw some sort of error?
      return FALSE;
    }

    $order_data = $this->getOrderDetails($order);

    $data = [
      'authority' => [
        'type' => 'checkout_id',
        'value' => $checkout_id,
      ],
      "reference" => $order->id(),
      "amount" => $order_data['amount'],
      "currency" => $order->getTotalPrice()->getCurrencyCode(),
      "capture" => TRUE,
      'order' => $order_data,
    ];

     //This works and raises a charge on the zipmoney platform... but a user can't have lots of pending payments so it might be useful to comment for testing
     $api_instance = new ChargesApi();
     $body = new CreateChargeRequest($data);
     $result = $api_instance->chargesCreate($body);

    /** @var \Drupal\commerce_checkout\Entity\CheckoutFlowInterface $checkout_flow */
    $checkout_flow = $order->get('checkout_flow')->entity;

    $payment_gateway_plugin = $commerce_payment_gateway->getPlugin();
    try {
      // Note that we're using a custom route instead of the payment return
      // one since the payment return callback cannot be called from the cart
      // page.

      if($shipping_profile = $order->getData('zipmoney_details')['shippingAddress']) {
        $shipments = $order->getData('zip_shipments');

        foreach ($shipments as $key => $shipment) {
          if ($original_amount = $shipment->getOriginalAmount()) {
            $shipment->setAmount($original_amount);
          }
          $shipment->clearAdjustments();
          $shipment->order_id->entity = $order;
          $rates = $this->shipmentManager->calculateRates($shipment);

          // There is no rates for shipping. "clear" the rate...
          // Note that we don't remove the shipment to prevent data loss (we're
          // mainly interested in preserving the shipping profile).
          if (empty($rates)) {
            $shipment->clearRate();
            continue;
          }
          $rate = $this->shipmentManager->selectDefaultRate($shipment, $rates);
          $this->shipmentManager->applyRate($shipment, $rate);

          /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
          $shipment->setShippingProfile($shipping_profile);
          $shipment->save();

        }
      }

      $order->shipments = $shipments;

      $order->set('checkout_step', 'complete');
      $order->save();

      $payment_gateway_plugin->onReturn($order, $request, $commerce_payment_gateway);

      $checkout_flow->set('order', $order);
      $checkout_flow->getPlugin()->redirectToStep('complete');

    } catch (PaymentGatewayException $e) {
      // When the payment fails, we don't instruct the JS to redirect, the page
      // will be reloaded to show errors.
      $this->logger->error($e->getMessage());
      $this->messenger->addError(t('Payment failed at the payment server. Please review your information and try again.'));
      return new JsonResponse();
    }
    return $result;

  }


  private function getShopperDetails($order) {
    if ($order->getData('zipmoney_details')) {
      $billing_details = $order->getData('zipmoney_details')['billingAddress'];
      $shopper = [
        "first_name" => $billing_details->given_name,
        "last_name" => $billing_details->family_name,
        'billing_address' => [
          'line1' => $billing_details->address_line1,
          'line2' => $billing_details->address_line2,
          'city' => $billing_details->locality,
          'state' => $billing_details->administrative_area,
          'postal_code' => $billing_details->postal_code,
          'country' => $billing_details->country_code,
          'first_name' => $billing_details->given_name,
          'last_name' => $billing_details->family_name,
        ],
      ];
      if ($order->getEmail()) {
        $shopper['email'] = $order->getEmail();
      }
      else {
        $shopper['email'] = $order->getData('zipmoney_details')['Email'];
      }
    }
    return $shopper;
  }

  private function getOrderDetails($order) {
    $zip_order = [
      "reference" => $order->id(),
      "amount" => round($order->getTotalPrice()->getNumber(), 2),
      "currency" => $order->getTotalPrice()->getCurrencyCode(),
    ];

    foreach ($order->getItems() as $item) {
      $zip_order['items'][] = [
        'name' => $item->getTitle(),
        "amount" => round($item->getUnitPrice()->getNumber(), 2),
        "reference" => $item->getPurchasedEntity()->getSku(),
        "quantity" => (int) $item->getQuantity(),
        "type" => "sku",
      ];
    }

    if (!empty($order->shipments->getValue())) {
      foreach ($order->shipments->getValue() as $shipment_id) {
        $shipment = Shipment::load($shipment_id['target_id']);
        $zip_order['items'][] = [
          'name' => 'Shipping ' . $shipment->getShippingMethod()->getName(),
          "amount" => round($shipment->getAmount()->getNumber()),
          "reference" => 'shipping',
          "quantity" => 1,
          "type" => "sku",
        ];
      }
    }
    elseif ($shipment = $order->getData('zipmoney_details')['shippingMethod']) {
      $zip_order['items'][] = [
        'name' => 'Shipping ' . $shipment->getName(),
        "amount" => (float) $shipment->getPlugin()
          ->getConfiguration()['rate_amount']['number'],
        "reference" => 'shipping',
        "quantity" => 1,
        "type" => "sku",
      ];
      // The shipping won't be included on the order total.
      $zip_order['amount'] = $zip_order['amount'] + $shipment->getPlugin()
          ->getConfiguration()['rate_amount']['number'];
    }

    return $zip_order;
  }

  /**
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   */
  public function SaveBilling(OrderInterface $order, Request $request) {
    $body = Json::decode($request->getContent());

    $billingAddress = $this->extractAddress($order, $body['billingInfo']);
    $shippingAddress = $this->extractAddress($order, $body['ShippingInfo']);
    $Email = $body['Email'];

    if ($body['shippingMethod']) {
      $shippingMethod = ShippingMethod::load((int) str_replace('--default', '', $body['shippingMethod']));
    }

    $order->setData('zipmoney_details', [
      'billingAddress' => $billingAddress,
      'shippingAddress' => $shippingAddress,
      'Email' => $Email,
      'shippingMethod' => isset($shippingMethod) ? $shippingMethod : NULL,
    ]);

    $order->save();
  }

  /**
   * Extracts the billing address from the request body.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Drupal\address\AddressInterface|null
   *   The address, NULL if empty.
   */
  protected function extractAddress(OrderInterface $order, $body) {

    // If the "profile copy" checkbox is checked, attempt to use the
    // shipping profile as the source of the address.
    if (!empty($body['profileCopy'])) {
      $profiles = $order->collectProfiles();

      if (isset($profiles['shipping']) && !$profiles['shipping']->get('address')
          ->isEmpty()) {
        return $profiles['shipping']->get('address')->first();
      }
    }
    /** @var \Drupal\profile\ProfileStorageInterface $profile_storage */
    $profile_storage = $this->entityTypeManager->getStorage('profile');
    if (!empty($body['profile'])) {
      // When "_original" is passed, attempt to load/use the default profile.
      if ($body['profile'] === '_original') {
        $profile = $profile_storage->loadByUser($order->getCustomer(), 'customer');
      }
      else {
        $profile = $profile_storage->load($body['profile']);
      }

      if ($profile && !$profile->get('address')->isEmpty()) {
        //return $profile->get('address')->getValue()[0];
        return $profile;
      }
    }
    elseif (!empty($body['address'])) {
      $profile = $profile_storage->create([
        'type' => 'customer',
        'address' => $body['address'],
      ]);
      //return $profile->get('address')->getValue()[0];
      return $profile;
    }

    return NULL;
  }

}
