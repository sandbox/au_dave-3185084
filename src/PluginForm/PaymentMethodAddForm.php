<?php

namespace Drupal\commerce_zipmoney\PluginForm;

use Drupal\commerce_quickpay_gateway\CurrencyCalculator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;


class PaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'commerce_zipmoney/zipmoney';

    $payment_method = $this->entity;

    $gateway_id = $payment_method->getPaymentGateway()->id();

    $order = \Drupal::routeMatch()->getParameter('commerce_order');

    $form['#attached']['drupalSettings']['commerceZipMoney'] = [
      'order_id' => $order->id(),
      'gateway_id' => $gateway_id,
    ];

    $form['#attributes']['class'][] = 'zipmoney-form';

    $form['zipmoney_button'] = [
      '#type' => 'container',
      '#id' => 'zipmoney-button-container',
      '#value' => ' zipMoney'
    ];
    $form['paypal_button']['button'] = [
      '#type' => 'markup',
      '#markup' => '<a id="zipmoney-button" href="#">Pay with <img src="https://static.zipmoney.com.au/assets/default/footer-tile/footer-tile-new.png" style="height:30px;"></a>',
    ];


    return $form;
  }





}
