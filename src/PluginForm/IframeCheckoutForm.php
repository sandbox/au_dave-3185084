<?php

namespace Drupal\commerce_zipmoney\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_quickpay_gateway\CurrencyCalculator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

class IframeCheckoutForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
//    return parent::create($container);
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => 'Pay me'
  ];
  }
}


