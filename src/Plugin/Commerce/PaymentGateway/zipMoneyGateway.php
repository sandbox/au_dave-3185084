<?php

namespace Drupal\commerce_zipmoney\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\commerce_order\Entity\OrderInterface;

use Symfony\Component\HttpFoundation\Request;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the QuickPay offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "zipmoney_payment_gateway",
 *   label = @Translation("Zipmoney payment Gateway"),
 *   create_label = @Translation("Zipmoney"),
 *   display_label = @Translation("Zipmoney"),
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_zipmoney\PluginForm\PaymentMethodAddForm"
 *   },
 *   requires_billing_information = TRUE,
 *   payment_method_types = {"zipmoney_paymentmethod"},
 * )
 */
class zipMoneyGateway extends OnsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Public Key'),
      '#default_value' => $this->configuration['api_public_key'],
      '#required' => TRUE,
    ];
    $form['api_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Private Key'),
      '#default_value' => $this->configuration['api_private_key'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['api_public_key'] = $values['api_public_key'];
      $this->configuration['api_private_key'] = $values['api_private_key'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $this->authorizePayment($payment);
    $this->assertAuthorized($payment);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $payment->setState('completed');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function receivePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed']);

    // If not specified, use the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $payment->state = 'paid';
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // There is no remote system.  These are only stored locally.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $payment_method->save();
  }

  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $_GET["checkoutId"],
      'remote_state' => $_GET["result"],
    ]);
    $payment->save();

  }

}
