<?php

namespace Drupal\commerce_zipmoney\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the Purchase Order payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "zipmoney_paymentmethod",
 *   label = @Translation("Zipmoney"),
 *   create_label = @Translation("Zipmoney"),
 *
 * )
 */
class zipMoneyPaymentMethodType extends PaymentMethodTypeBase {



  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return 'test 2';
  }
}
