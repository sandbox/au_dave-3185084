/**
 * @file
 * Defines behaviors for the Braintree payment method form.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  var initialized;
  var $form = $('form.commerce-checkout-flow');
  var $submit = $form.find('.button--primary');
  var $shipping_info;
  // var $data = Drupal.paypalCheckout.extractBillingInfo($form);


  function init(context, settings) {
    if (!initialized) {
      initialized = true;

      $submit.attr('disabled',true)

      var waitforZip = setInterval(function () {
        if (typeof Zip !== 'undefined') {
          clearInterval(waitforZip);
          $('#zipmoney-button').off('click');
          $('#zipmoney-button').on('click', function (event) {
            event.preventDefault();
            var validateForm = Drupal.behaviors.commerceZipMoney.validateForm($form, event);
            if(!validateForm) {
              return;
            }
            var billingInfo = Drupal.behaviors.commerceZipMoney.extractBillingInfo($form);
            var $shippingMethod = Drupal.behaviors.commerceZipMoney.extractShippingMethod($form);
            var ShippingInfo = Drupal.behaviors.commerceZipMoney.extractShippingInfo($form);
            var Email = Drupal.behaviors.commerceZipMoney.extractEmail($form);
            var $data = {
              billingInfo: billingInfo,
              ShippingInfo: ShippingInfo,
              shippingMethod: $shippingMethod,
              Email: Email,
            };

            console.log($data);

            Drupal.behaviors.commerceZipMoney.makeCall('/commerce-zipmoney/save-billing/' + settings.commerceZipMoney.gateway_id + '/'  + settings.commerceZipMoney.order_id , {
              type: 'POST',
              contentType: "application/json; charset=utf-8",
              data: JSON.stringify($data)
            })

            Zip.Checkout.init({
              checkoutUri: '/commerce-zipmoney/checkout/' + settings.commerceZipMoney.gateway_id + '/' + settings.commerceZipMoney.order_id,
              redirectUri: '/commerce-zipmoney/' + settings.commerceZipMoney.gateway_id + '/' + settings.commerceZipMoney.order_id + '/return',
              onError: function (args) {
              }
            })
          });
        }
      }, 100);
    }


  }




  Drupal.behaviors.commerceZipMoney = {

    attach: function (context, settings) {
      init(context, settings);
      console.log('zip attach');
      $submit.prop('disabled', true);
    },
    detach: function(context, settings) {
      console.log('zip dettach');
      initialized = false;
    },
    validateForm: function($form, event) {
      var form = document.querySelector('form');
      var validity = form.reportValidity();
      console.log(validity);
      if($(':input[name$="[email_confirm]"]', $form).length > 0) {
        if($(':input[name$="[email]"]', $form).val() != $(':input[name$="[email_confirm]"]', $form).val()) {
          $(':input[name$="[email_confirm]"]', $form).after('<span class="error">The specified emails don\'t match</span>')
          validity = false;
        }
      }
      console.log(validity);
      return validity;

    },
    extractEmail: function($form) {
      var EmailTextBox = $(':input[name$="[email]"]', $form);
      return EmailTextBox.val();
    },

    extractShippingMethod: function($form) {
      var shippingMethodCheckbox = $(':input[name$="[shipments][0][shipping_method][0]"][checked="checked"]', $form);
      console.log(shippingMethodCheckbox);
      return shippingMethodCheckbox.val();
    },

    extractBillingInfo: function ($form) {
      var billingInfo = {
        profile: null,
        address: {},
        profileCopy: false
      };

      // Check if the "profile copy" checkbox is present and checked. If so,
      // we first need to check if the shipping information pane is present in
      // the page. If it is, we need to use the address selected/entered.
      // In case the pane isn't present, we'll try to get the collect the
      // shipping profile using $order->collectProfiles().
      var $profileCopyCheckbox = $(':input[name$="[billing_information][copy_fields][enable]"]', $form);
      if ($profileCopyCheckbox.length && $profileCopyCheckbox.is(':checked')) {
        var shippingInfo = this.extractShippingInfo($form);
        if (shippingInfo.profile) {
          billingInfo.profile = shippingInfo.profile;
        }
        else if (!$.isEmptyObject(shippingInfo.address)) {
          billingInfo.address = shippingInfo.address;
        }
        else {
          billingInfo.profileCopy = true;
        }
        return billingInfo;
      }

      // Extract the billing information from the selected profile.
      $form.find(':input[name*="[billing_information][address][0][address]"]').each(function() {
        // Extract the address field name.
        var name = jQuery(this).attr('name').split('[');
        name = name[name.length - 1];
        billingInfo.address[name.substring(0, name.length - 1)] = $(this).val();
      });

      // Fallback to the entered address, if the address fields are present.
      var $addressSelector = $('select[name*="[billing_information][select_address]"]', $form);
      if ($.isEmptyObject(billingInfo.address) && ($addressSelector.length && $addressSelector.val() !== '_new')) {
        billingInfo.profile = $addressSelector.val();
      }

      return billingInfo;
    },
    extractShippingInfo: function ($form) {
      var shippingInfo = {
        profile: null,
        address: {}
      };

      $form.find(':input[name*="[shipping_profile][address][0][address]"]').each(function() {
        // Extract the address field name.
        var name = jQuery(this).attr('name').split('[');
        name = name[name.length - 1];
        shippingInfo.address[name.substring(0, name.length - 1)] = $(this).val();
      });

      var $addressSelector = $('select[name="shipping_information[shipping_profile][select_address]"', $form);
      if ($.isEmptyObject(shippingInfo.address) && ($addressSelector.length && $addressSelector.val() !== '_new')) {
        shippingInfo.profile = $addressSelector.val();
      }

      return shippingInfo;
    },
    makeCall: function(url, settings) {
      console.log(url);
      console.log(settings);
      settings = settings || {};
      var ajaxSettings = {
        dataType: 'json',
        url: url,
        async: false,
      };
      $.extend(ajaxSettings, settings);
      return $.ajax(ajaxSettings);
    },


  }

})(jQuery, Drupal, drupalSettings)
