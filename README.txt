This is module is intended to add allow payments from ZipMoney https://zip.co/ within a drupal, drupal commerce setup.

You will need to have a ZipMoney merchant account, then add it as a payment gateway.

After that it should appear as an option on your 'payment information' pane during the checkout flow.

Disclaimer:
I worked on this while working on a site that required Zipmoney integration, I am no longer working on this site, so ongoing updates are unlikely. It is also my first attempt at creating a commerce payment gateway.
I am happy to pass on ownership of this module, or you can download it and use it as inspiration.
